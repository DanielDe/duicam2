//
//  FeedbackViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/23/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

#pragma mark - Public methods

- (void)setTitle:(NSString *)title andDescription:(NSString *)description {
	[self setTitle:title];
	[self setDescription:description];
}

- (void)setTitle:(NSString *)title {
	titleLabel.text = title;
}

- (void)setDescription:(NSString *)description {
	descriptionLabel.text = description;
}

- (void)startAnimating {
	self.view.alpha = 1.0;
	checkmarkImageView.hidden = YES;
	[activityIndicator startAnimating];
}

- (void)stopAnimating {
	self.view.alpha = 0.0;
	[activityIndicator stopAnimating];
}

- (void)checkAndFade {
	self.view.alpha = 1.0;
	[activityIndicator stopAnimating];
	checkmarkImageView.hidden = NO;
	
	// fade the view out 
	[UIView animateWithDuration:0.5
						  delay:2
						options:0
					 animations:^{
						 self.view.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {}];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

	// give the fvc rounded corners
	self.view.layer.cornerRadius = 30.0;
	
	// hide the fvc
	self.view.alpha = 0.0;
    
    self.view.frame = CGRectMake(0, 0, 200, 120);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	titleLabel = nil;
	descriptionLabel = nil;
	checkmarkImageView = nil;
	activityIndicator = nil;
	[super viewDidUnload];
}
@end
