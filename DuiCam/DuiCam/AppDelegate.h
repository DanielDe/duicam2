//
//  AppDelegate.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
