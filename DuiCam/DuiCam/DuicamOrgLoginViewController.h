//
//  DuicamOrgLoginViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/22/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFURLSessionManager.h>
#import "AFHTTPRequestOperationManager.h"
#import "SettingsViewController.h"
#import "constants.h"
#import "functions.h"

@interface DuicamOrgLoginViewController : UIViewController {

    
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UILabel *confirmPasswordLabel;
    __weak IBOutlet UITextField *confirmPasswordTextField;
    __weak IBOutlet UILabel *confirmationCodeLabel;
    __weak IBOutlet UITextField *confirmationCodeTextField;
    __weak IBOutlet UIButton *registerButton;
    __weak IBOutlet UIButton *alreadyHaveAccountButton;
    __weak IBOutlet UILabel *errorLabel;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet UILabel *termsConditionsLabel;
    __weak IBOutlet UISwitch *termsConditionsSwitch;
    
    BOOL isRegisterMode;
}

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)registerButtonPressed:(id)sender;
- (IBAction)alreadyHaveAccountButtonPressed:(id)sender;

@end
