//
//  SettingsViewController.m
//  DuiCam
//
//  Created by Daniel de Haas on 1/22/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () {
	#define kLastUsedPresetKey @"kLastUsedPresetKey"
	#define kClipLengthKey @"kClipLength"
	#define kReceivingEmailUpdates @"kReceivingEmailUpdates"
	#define kUpdatesEmailAddress @"kUpdatesEmailAddress"
	
	// table views
	UITableView *mainTableView;
	UITableView *videoQualityTableView;
	UITableView *clipLengthTableView;
	UITableView *emailUpdatesTableView;
	
	// data sources
	NSMutableArray *videoQualities;
	NSMutableArray *clipLengths;
	
	// text fields
	UITextField *emailTextField;
    
    FeedbackViewController *fvc;
}

@end

@implementation SettingsViewController

#pragma mark - Utilities

- (BOOL)isIOS7 {
    return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0;
}

#pragma mark - MFMailComposeViewControllerDelegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (tableView == mainTableView) {
		switch (indexPath.section) {
			case 0:
			{
                // Calculate table view height based on OS.
                double navigationBarHeight = 64.0; // Guaranteed to be accurate.
                double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - ([self isIOS7] ? 0 : navigationBarHeight);
                
                // Calculate note label offset according to OS.
                double noteLabelOffset = [self isIOS7] ? 20 : 0;

				if (indexPath.row == 0) {
					UIViewController *viewController = [[UIViewController alloc] init];
					[viewController setTitle:@"Video Quality"];
					
					videoQualityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStyleGrouped];
					videoQualityTableView.dataSource = self;
					videoQualityTableView.delegate = self;
                    
					// add note label
					UILabel *noteLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 75)];
					noteLabel.text = @"Higher quality recordings use more disk space.";
					noteLabel.font = [UIFont boldSystemFontOfSize:15.0];
					noteLabel.textColor = [UIColor colorWithRed:0.223 green:0.266 blue:0.349 alpha:1.0];
					noteLabel.shadowColor = [UIColor whiteColor];
					noteLabel.shadowOffset = CGSizeMake(1.0, 1.0);
					noteLabel.numberOfLines = 10;
					noteLabel.backgroundColor = [UIColor clearColor];
					noteLabel.textAlignment = UITextAlignmentCenter;
					noteLabel.center = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, 170 + noteLabelOffset);
					[videoQualityTableView addSubview:noteLabel];
					
					[viewController.view addSubview:videoQualityTableView];
					
					[self pushViewController:viewController animated:YES];
				} else {
					UIViewController *viewController = [[UIViewController alloc] init];
					[viewController setTitle:@"Clip Length"];
					
					clipLengthTableView	= [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStyleGrouped];
					clipLengthTableView.dataSource = self;
					clipLengthTableView.delegate = self;
					
					// add note label
					UILabel *noteLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 75)];
					noteLabel.text = @"DuiCam stores three temporary clips of the above length. Longer clip lengths use more disk space.";
					noteLabel.font = [UIFont boldSystemFontOfSize:15.0];
					noteLabel.textColor = [UIColor colorWithRed:0.223 green:0.266 blue:0.349 alpha:1.0];
					noteLabel.shadowColor = [UIColor whiteColor];
					noteLabel.shadowOffset = CGSizeMake(1.0, 1.0);
					noteLabel.numberOfLines = 3;
					noteLabel.backgroundColor = [UIColor clearColor];
					noteLabel.textAlignment = UITextAlignmentCenter;
					noteLabel.center = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, 310 + noteLabelOffset);
					[clipLengthTableView addSubview:noteLabel];
					
					[viewController.view addSubview:clipLengthTableView];
					
					clipLengths = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:60],
								   [NSNumber numberWithInt:180],
								   [NSNumber numberWithInt:300],
								   [NSNumber numberWithInt:600],
								   [NSNumber numberWithInt:900],
								   [NSNumber numberWithInt:1200],
								   nil];
					
					[self pushViewController:viewController animated:YES];
				}
			}
				break;
			case 1:
			{
                // Calculate table view height based on OS.
                double navigationBarHeight = 64.0; // Guaranteed to be accurate.
                double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - ([self isIOS7] ? 0 : navigationBarHeight);
                
				UIViewController *viewController = [[UIViewController alloc] init];
				[viewController setTitle:@"Email Updates"];
				emailUpdatesTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStyleGrouped];
				emailUpdatesTableView.dataSource = self;
				emailUpdatesTableView.delegate = self;
				[viewController.view addSubview:emailUpdatesTableView];
				
				[self pushViewController:viewController animated:YES];
			}
				break;
			case 2:
			{
                // Present a register/login view if the user is not logged in, or log the user out if they are logged in.
                NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:DUICAM_USERNAME_KEY];
                if (!username) {
                    DuicamOrgLoginViewController *duicamLogin = [[DuicamOrgLoginViewController alloc] init];
                    [self presentViewController:duicamLogin animated:YES completion:nil];
                } else {
                    // Remove user's login and username from NSUserDefaults
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:DUICAM_USERNAME_KEY];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:DUICAM_PASSWORD_KEY];
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
			}
				break;
            case 3:
			{
                // Calculate table view height based on OS.
                double navigationBarHeight = 64.0; // Guaranteed to be accurate.
                double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - navigationBarHeight;
                double navigationBarOffset = [self isIOS7] ? navigationBarHeight : 0;
                
				NSString *path;
				NSString *title;
				if (indexPath.row == 0) {
					path = [[NSBundle mainBundle] pathForResource:@"LicenseAgreement" ofType:@"txt"];
					title = @"License Agreement";
				} else {
					path = [[NSBundle mainBundle] pathForResource:@"About" ofType:@"txt"];
					title = @"About";
				}
				
				NSString *contents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
				
				UIViewController *viewController = [[UIViewController alloc] init];
				[viewController setTitle:title];
				
				UITableView *background = [[UITableView alloc] initWithFrame:CGRectMake(0, navigationBarOffset, 320, tableViewHeight) style:UITableViewStyleGrouped];
				[background setScrollsToTop:NO];
				[background setDelegate:self];
				[viewController.view addSubview:background];
                
				UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, navigationBarOffset, 320, tableViewHeight)];
				[textView setEditable:NO];
				[textView setScrollsToTop:YES];
				[textView setBackgroundColor:[UIColor clearColor]];
				[textView setFont:[UIFont systemFontOfSize:14]];
				[textView setText:contents];
				[viewController.view addSubview:textView];
				
				[self pushViewController:viewController animated:YES];
			}
				break;
			case 4:
			{
				// email
				if (![MFMailComposeViewController canSendMail]) {
					[[[UIAlertView alloc] initWithTitle:@"Error" message:@"This device is not currently set up to send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
					return;
				}
				MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
				mailComposer.mailComposeDelegate = self;
				[mailComposer setToRecipients:[NSArray arrayWithObject:@"eslucr@gmail.com"]];
				
				if (indexPath.row == 1) {
					[mailComposer setSubject:@"DuiCam for iPhone Bug Report"];
				}
				
				[self presentViewController:mailComposer animated:YES completion:nil];
				// [self presentModalViewController:mailComposer animated:YES];]
			}
				break;
			case 5:
			{
                // Calculate table view height based on OS.
                double navigationBarHeight = 64.0; // Guaranteed to be accurate.
                double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - navigationBarHeight;
                double navigationBarOffset = [self isIOS7] ? navigationBarHeight : 0;
				
				NSString *title = @"Gifting Instructions";
				
				NSString *contents = @"For information on supporting this research, please visit DuiCam.org";
				
				UIViewController *viewController = [[UIViewController alloc] init];
				[viewController setTitle:title];
				
				UITableView *background = [[UITableView alloc] initWithFrame:CGRectMake(0, navigationBarOffset, 320, tableViewHeight) style:UITableViewStyleGrouped];
				[background setScrollsToTop:NO];
				[background setDelegate:self];
				[viewController.view addSubview:background];
				
				UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, navigationBarOffset, 320, tableViewHeight)];
				[textView setEditable:NO];
				[textView setScrollsToTop:YES];
				[textView setBackgroundColor:[UIColor clearColor]];
				[textView setFont:[UIFont systemFontOfSize:14]];
				[textView setText:contents];
				[viewController.view addSubview:textView];
				
				[self pushViewController:viewController animated:YES];
			}
				break;
			default:
				break;
		}
	} else if (tableView == videoQualityTableView) {
		//ViewController *presenter = (ViewController *)[self presentingViewController];
		switch (indexPath.row) {
			case 0:
			{
				[[NSUserDefaults standardUserDefaults] setObject:AVCaptureSessionPresetHigh forKey:kLastUsedPresetKey];
				
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] setAccessoryType:UITableViewCellAccessoryCheckmark];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
			}
				break;
			case 1:
			{
				[[NSUserDefaults standardUserDefaults] setObject:AVCaptureSessionPresetMedium forKey:kLastUsedPresetKey];
				
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]] setAccessoryType:UITableViewCellAccessoryCheckmark];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
			}
				break;
			case 2:
			{
				[[NSUserDefaults standardUserDefaults] setObject:AVCaptureSessionPresetLow forKey:kLastUsedPresetKey];
				
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
				[[videoQualityTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]] setAccessoryType:UITableViewCellAccessoryCheckmark];
			}
				break;
			default:
				break;
		}
		
		[mainTableView reloadData];
	} else if (tableView == clipLengthTableView) {
		[[NSUserDefaults standardUserDefaults] setInteger:[[clipLengths objectAtIndex:indexPath.row] intValue] forKey:kClipLengthKey];
		
		for (int i = 0; i < [clipLengths count]; ++i) {
			if (i == indexPath.row) {
				[[clipLengthTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]] setAccessoryType:UITableViewCellAccessoryCheckmark];
			} else {
				[[clipLengthTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];
			}
		}
		
		//ViewController *presenter = (ViewController *)[self presentingViewController];
		[mainTableView reloadData];
	} else if (tableView == emailUpdatesTableView) {
		if (indexPath.section == 1) {
			NSString *email = @"";
			int sendEmailUpdates;
			if ([[NSUserDefaults standardUserDefaults] boolForKey:kReceivingEmailUpdates]) {
				email = [[NSUserDefaults standardUserDefaults] objectForKey:kUpdatesEmailAddress];
				sendEmailUpdates = 0;
				[[NSUserDefaults standardUserDefaults] setBool:NO forKey:kReceivingEmailUpdates];
			} else {
				email = emailTextField.text;
				if (!email || [email isEqualToString:@""]) {
					[[[UIAlertView alloc] initWithTitle:@"Invalid Email" message:@"You have entered an invalid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
					return;
				}
				sendEmailUpdates = 1;
				[[NSUserDefaults standardUserDefaults] setObject:email forKey:kUpdatesEmailAddress];
				[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kReceivingEmailUpdates];
			}
			
			// update the Google doc
			email = [email stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
			NSString *payload = [NSString stringWithFormat:@"%@%@%@%d%@", @"entry.0.single=", email, @"&entry.1.single=", sendEmailUpdates, @"&pageNumber=0&backupCache="];
			// URL to form response
			NSURL *url = [[NSURL alloc] initWithString:@"https://docs.google.com/spreadsheet/formResponse?formkey=dGNjMHBqOWVUUE1yc2NDbU1raktDNHc6MQ"];
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
			NSData *postData = [payload dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
			NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
			[request setHTTPMethod:@"POST"];
			[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
			[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
			[request setHTTPBody:postData];
			NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
			[connection start];
			
			[mainTableView reloadData];
			[self popToRootViewControllerAnimated:YES];
		}
	}
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == mainTableView) {
		switch (indexPath.section) {
			case 0:
			{
				static NSString *identifier = @"optionsReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
				}
				
				if (indexPath.row == 0) {
					cell.textLabel.text = @"Video quality";
					
					NSString *currentPreset = [[NSUserDefaults standardUserDefaults] objectForKey:kLastUsedPresetKey];
					if ([currentPreset isEqualToString:AVCaptureSessionPresetMedium])
						currentPreset = @"Medium";
					else if ([currentPreset isEqualToString:AVCaptureSessionPresetLow])
						currentPreset = @"Low";
					else if (!currentPreset || [currentPreset isEqualToString:AVCaptureSessionPresetHigh])
						currentPreset = @"High";
					else
						NSLog(@"Error: session preset isn't standard");
					
					cell.detailTextLabel.text = currentPreset;
				} else {
					cell.textLabel.text = @"Clip length";
					
					int currentClipLength = [[NSUserDefaults standardUserDefaults] integerForKey:kClipLengthKey];
					if (currentClipLength == 0) {
						currentClipLength = 180;
						[[NSUserDefaults standardUserDefaults] setInteger:currentClipLength forKey:kClipLengthKey];
					}
					if (currentClipLength < 60) {
						cell.detailTextLabel.text = [NSString stringWithFormat:@"%d seconds", currentClipLength];
					} else if (currentClipLength == 60) {
						int numMinutes = currentClipLength / 60;
						cell.detailTextLabel.text = [NSString stringWithFormat:@"%d minute", numMinutes];
					} else {
						int numMinutes = currentClipLength / 60;
						cell.detailTextLabel.text = [NSString stringWithFormat:@"%d minutes", numMinutes];
					}
				}
				
				return cell;
			}
				break;
			case 1:
			{
				static NSString *identifier = @"emailReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
				}
				
				cell.textLabel.text = @"Receive email updates";
				cell.selectionStyle = UITableViewCellSelectionStyleNone;
				[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
				if ([[NSUserDefaults standardUserDefaults] boolForKey:kReceivingEmailUpdates]) {
					NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:kUpdatesEmailAddress];
					cell.detailTextLabel.text = [NSString stringWithFormat:@"Emails sent to %@", email];
				} else {
					cell.detailTextLabel.text = @"Updates not being sent";
				}
				
				return cell;
			}
				break;
            case 2:
			{
				static NSString *identifier = @"duicamOrgReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				}
				
                NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:DUICAM_USERNAME_KEY];
                if (!email) {
                    cell.textLabel.text = @"Register on DuiCam.org";
                } else {
                    cell.textLabel.text = @"Log out of DuiCam.org";
                }
				
				return cell;
			}
				break;
			case 3:
			{
				static NSString *identifier = @"aboutReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				}
				
				if (indexPath.row == 0)
					cell.textLabel.text = @"License agreement";
				else
					cell.textLabel.text = @"About";
				
				return cell;
			}
				break;
			case 4:
			{
				static NSString *identifier = @"contactReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				}
				
				if (indexPath.row == 0)
					cell.textLabel.text = @"Contact us";
				else
					cell.textLabel.text = @"Report bug/issue";
				
				return cell;
			}
				break;
			case 5:
			{
				static NSString *identifier = @"donateReuseIdentifier";
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
				if (cell == nil) {
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				}
				
				cell.textLabel.text = @"Gifting Instructions";
				
				return cell;
			}
				break;
			default:
				break;
		}
	} else if (tableView == videoQualityTableView) {
		NSString *currentPreset = [[NSUserDefaults standardUserDefaults] objectForKey:kLastUsedPresetKey];
		int currentPresetIndex = 0;
		if ([currentPreset isEqualToString:AVCaptureSessionPresetMedium])
			currentPresetIndex = 1;
		else if ([currentPreset isEqualToString:AVCaptureSessionPresetLow])
			currentPresetIndex = 2;
		else if (!currentPreset || [currentPreset isEqualToString:AVCaptureSessionPresetHigh])
			currentPresetIndex = 0;
		else
			NSLog(@"Error: session preset isn't standard");
		
		static NSString *identifier = @"videoQualitiesReuseIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
		}
		
		if (indexPath.row == currentPresetIndex)
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		else
			[cell setAccessoryType:UITableViewCellAccessoryNone];
		
		switch (indexPath.row) {
			case 0:
			{
				cell.textLabel.text = @"High";
			}
				break;
			case 1:
			{
				cell.textLabel.text = @"Medium";
			}
				break;
			case 2:
			{
				cell.textLabel.text = @"Low";
			}
				break;
			default:
				break;
		}
		
		return cell;
	} else if (tableView == clipLengthTableView) {
		int currentClipLength = [[NSUserDefaults standardUserDefaults] integerForKey:kClipLengthKey];
		
		static NSString *identifier = @"clipLengthsReuseIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
		}
		
		int nextClipLength = [[clipLengths objectAtIndex:indexPath.row] intValue];
		if (nextClipLength == currentClipLength)
			[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
		
		if (nextClipLength < 60) {
			cell.textLabel.text = [NSString stringWithFormat:@"%d seconds", nextClipLength];
		} else if (nextClipLength == 60) {
			int numMinutes = nextClipLength / 60;
			cell.textLabel.text = [NSString stringWithFormat:@"%d minute", numMinutes];
		} else {
			int numMinutes = nextClipLength / 60;
			cell.textLabel.text = [NSString stringWithFormat:@"%d minutes", numMinutes];
		}
		
		return cell;
	} else if (tableView == emailUpdatesTableView) {
		if (indexPath.section == 0) {
			static NSString *identifier = @"emailUpdatesReuseIdentifier";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				cell.accessoryType = UITableViewCellAccessoryNone;
				
				if ([indexPath section] == 0) {
					emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 185, 30)];
					emailTextField.adjustsFontSizeToFitWidth = YES;
					emailTextField.textColor = [UIColor blackColor];
					if ([[NSUserDefaults standardUserDefaults] boolForKey:kReceivingEmailUpdates]) {
						emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kUpdatesEmailAddress];
						emailTextField.enabled = NO;
						emailTextField.clearButtonMode = UITextFieldViewModeNever;
					} else {
						emailTextField.placeholder = @"email@example.com";
						emailTextField.enabled = YES;
						emailTextField.clearButtonMode = UITextFieldViewModeAlways;
					}
					emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
					emailTextField.returnKeyType = UIReturnKeyDone;
					emailTextField.backgroundColor = [UIColor clearColor];
					emailTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
					emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
					emailTextField.textAlignment = UITextAlignmentLeft;
					emailTextField.tag = 0;
					emailTextField.delegate = self;
					
					[cell addSubview:emailTextField];
				}
			}
			
			cell.textLabel.text = @"Email";
			
			return cell;
		} else {
			static NSString *identifier = @"emailUpdatesButtonReuseIdentifier";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
				cell.textLabel.textAlignment = UITextAlignmentCenter;
			}
			
			if ([[NSUserDefaults standardUserDefaults] boolForKey:kReceivingEmailUpdates]) {
				cell.textLabel.text = @"Stop Receiving Updates";
			} else {
				cell.textLabel.text = @"Start Receiving Updates";
			}
			
			return cell;
		}
	}
	return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (tableView == mainTableView) {
		return 6;
	} else if (tableView == videoQualityTableView) {
		return 1;
	} else if (tableView == clipLengthTableView) {
		return 1;
	} else if (tableView == emailUpdatesTableView) {
		return 2;
	}
	return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (tableView == mainTableView) {
		switch (section) {
			case 0:
			{
				return 2;
			}
				break;
			case 1:
			{
				return 1;
			}
				break;
            case 2:
			{
				return 1;
			}
				break;
			case 3:
			{
				return 2;
			}
				break;
			case 4:
			{
				return 2;
			}
				break;
			case 5:
			{
				return 1;
			}
				break;
			default:
				break;
		}
	} else if (tableView == videoQualityTableView) {
		return 3;
	} else if (tableView == clipLengthTableView) {
		return [clipLengths count];
	} else if (tableView == emailUpdatesTableView) {
		return 1;
	}
	return 0;
}

- (void)signinModalDismissed {
    [mainTableView reloadData];
    [fvc setTitle:@"Signed in" andDescription:@"Successfully signed in"];
    [fvc checkAndFade];
}

#pragma mark - Buttons

- (void)doneButtonPressed {
	// [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return NO;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
	// create the root view controller
	UIViewController *rootController = [[UIViewController alloc] init];
	rootController.navigationItem.title = @"Settings";
	
	// create the done bar button item for the root controller
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
	rootController.navigationItem.rightBarButtonItem = doneButton;
	
    // Calculate table view height based on OS.
    double navigationBarHeight = 64.0; // Guaranteed to be accurate.
    double tableViewHeight = [[UIScreen mainScreen] bounds].size.height - ([self isIOS7] ? 0 : navigationBarHeight);
    
	mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, tableViewHeight) style:UITableViewStyleGrouped];
	mainTableView.delegate = self;
	mainTableView.dataSource = self;
	
	// add the table view to the root controller
	[rootController.view addSubview:mainTableView];
	
	// make the root controller visible
	[self pushViewController:rootController animated:NO];
    
    // set up the fvc
	fvc = [[FeedbackViewController alloc] init];
    fvc.view.frame = CGRectMake(0, 0, 200, 200);
	fvc.view.center = CGPointMake(160, 240);
	[self.view addSubview:fvc.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
