//
//  ViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/13/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecorderViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "ThumbnailButton.h"
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>
#import "PlayerViewController.h"
#import "FileViewerViewController.h"
#import "SettingsViewController.h"
#import "FeedbackViewController.h"
#import <AFHTTPRequestOperationManager.h>
#import "constants.h"
#import "functions.h"

@interface ViewController : UIViewController <UIAlertViewDelegate> {
    __unsafe_unretained IBOutlet UIToolbar *toolbar;
}

- (IBAction)recordButtonPressed:(id)sender;
- (IBAction)foldersButtonPressed:(id)sender;
- (IBAction)settingsButtonPressed:(id)sender;

- (void)newClipSavedAtPath:(NSString *)path;

@end
