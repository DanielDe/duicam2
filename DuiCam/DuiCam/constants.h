//
//  constants.h
//  DuiCam
//
//  Created by Daniel de Haas on 2/23/14.
//  Copyright (c) 2014 Daniel de Haas. All rights reserved.
//

#ifndef DuiCam_constants_h
#define DuiCam_constants_h

#define DUICAM_ORG_URL @"http://ec2-23-20-103-249.compute-1.amazonaws.com/"
#define DUICAM_KEYCHAIN_ITEM_NAME @"DuiCam.org keychain"

#define DUICAM_USERNAME_KEY @"duicam_username_key"
#define DUICAM_PASSWORD_KEY @"duicam_password_key"

#endif
