//
//  SettingsViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/22/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ViewController.h"
#import "DuicamOrgLoginViewController.h"
#import "FeedbackViewController.h"
#import "constants.h"

@class ViewController;

@interface SettingsViewController : UINavigationController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UITextFieldDelegate, NSURLConnectionDelegate>

- (void)signinModalDismissed;

@end
