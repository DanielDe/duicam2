//
//  ScreenshotViewerViewController.h
//  DuiCam
//
//  Created by Daniel de Haas on 1/21/13.
//  Copyright (c) 2013 Daniel de Haas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ScreenshotViewerViewController : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate> {
	// IBOutlets
	IBOutlet UIScrollView *mainScrollView;
	IBOutlet UINavigationBar *topBar;
	IBOutlet UIToolbar *bottomBar;
}

// inits
- (id)initWithImage:(UIImage *)_image;

// IBActions
- (IBAction)shareButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)plusButtonPressed:(id)sender;
- (IBAction)minusButtonPressed:(id)sender;

@end
